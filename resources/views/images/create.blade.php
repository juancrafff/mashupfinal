@extends('layouts.master')

@section('titulo')
	Nueva consulta
@endsection

@section('contenido')
<div class="row">
	<div class="offset-md-3 col-md-6">
		<div class="card">
			<div class="card-header text-center">
				Analizar imagen
			</div>
			<div class="card-body" style="padding:30px">
				{{-- TODO: Abrir el formulario e indicar el método POST --}}
				<form method="post" action=" {{ route('images.store') }}" enctype="multipart/form-data">
					{{-- TODO: Protección contra CSRF --}}
					@csrf
					<div class="form-group">
						<label for="titulo">Título</label>
						<input name="titulo" class="form-control" type="text" required placeholder="Dale un título a tu foto">
					</div>
					<div class="row">
						
						<div class="col form-group">
							<label for="imagen">Elegir imagen</label>
							<input class="form-control-file" type="file" id="img" name="imagen" accept="image/*" required>
						</div>

						<div class="col form-group">
							<label for="confianza">Confianza</label>
							<input type="number" min="0" max="100" name="confianza" id="confianza" class="col form-control" required>
						</div>

					</div>
					
					<div class="form-group text-center">
						<button type="submit" class="btn btn-outline-success" style="padding:8px 100px;margin-top:25px;">
						Consultar </button>
					</div>
				</form>
				{{-- TODO: Cerrar formulario --}}
			</div>
		</div>
	</div>
</div>
@endsection