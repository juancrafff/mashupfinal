@extends('layouts.master')

@section('titulo')
Hashtag AI
@endsection

@section('contenido')
<div class="container">
	<div class="row hidden-md-up">
		@foreach( $imagenes as $imagen )
		<div class="col-md-4 animales">
			<a class="btn btn-outline-danger" href="{{ route('images.show' , $imagen) }}" style="text-decoration: none; color: inherit;">
				<div class="card" style="height:12em">
					<div class="card-title">{{$imagen->titulo}}</div>
					<div class="row">
						<div class="col">
							<img class="card-img" src="{{ asset("assets/imagenes/") }}/{{ $imagen->imagen }}" />
						</div>
					</div>		
				</div>
			</a>
		</div>
		@endforeach
	</div>
</div>
@endsection