@extends('layouts.master')

@section('titulo')
Transportistas
@endsection

@section('contenido')
<div class="container">
    <div class="row hidden-md-up">
		@foreach( $transportistas as $transportista )
		<div class="col-md-4 transportistas">
			<a class="btn btn-outline-danger" href="{{ route('transportistas.show' , $transportista) }}" style="text-decoration: none; color: inherit;">
				<div class="card" style="height:12em">
					<div class="row">
						<div class="col">
							<img class="card-img" src="{{ asset("assets/imagenes/transportistas") }}/{{ $transportista->imagen }}" />
						</div>
						<div class="col">
							<h4 class="card-title">{{$transportista->nombre}}</h4>
							<div class="card-block">
                                <small>{{$transportista->paquetes->where("entregado","0")->count()}} paquetes pendientes de entrega</small>
							</div>
						</div>
					</div>		
				</div>
			</a>
		</div>
		@endforeach
	</div>
</div>
@endsection