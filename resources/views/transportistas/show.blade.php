@extends('layouts.master')

@section('titulo')
Mostrar transportista
@endsection

@section('contenido')
<div class="container">
	<div class="row">
		<div class="col-sm-3">
			<img src="{{ asset("assets/imagenes/transportistas") }}/{{ $transportista->imagen }}" style="height:200px"/>
		</div>
		<div class="col-sm-9">
			<h1>{{ $transportista->nombre }} {{$transportista->apellidos}}</h1>
            <h4>Años de permiso de circulación: <small>{{$transportista->permiso()}}</small></h4>

            <h4>Empresas:</h4>
            <ul class="list-group">
				@foreach($transportista->empresas as $empresa)
				<li class="list-group-item">{{$empresa->nombre}}</li>
				@endforeach
			</ul>

            
            <h4>Paquetes:</h4>
            <ol class="fa">
				@foreach($transportista->paquetes->where("entregado","0") as $paquete)
				<li class="">{{$paquete->dir_entrega}}</li>
				@endforeach
			</ol>

			<div class="row col-8">
				<div class="col">
					<a href="" class="btn btn-warning">Entregar todo</a>
				</div>
				<div class="col">
					<a href="" class="btn btn-outline-success">Marcar como no entregado</a>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection