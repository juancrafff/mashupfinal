@extends('layouts.master')

@section('titulo')
Crear paquete
@endsection

@section('contenido')
<div class="container">
  <h4 class="text-center text-uppercase">Nuevo paquete</h4>
    <form action="post" action="{{ route('paquetes.store')}}" enctype="multipart/form-data">
        @csrf
        <div class="form-group">
          <label for="direccion">Dirección de entrega</label>
          <input type="text" name="direccion" class="form-control" placeholder="Dóndo lo envías?">
        </div>
        <label for="transportista">Elige el transportista</label>
        <select class="form-control" name="transportista" id="">
          @foreach ($transportistas as $transportista)
          <option value="{{$transportista->id}}">{{$transportista->nombre}}</option>
          @endforeach
        </select>
        <button type="submit" class="btn btn-primary" name="crear">Crear envío</button>
    </form>
</div>
@endsection