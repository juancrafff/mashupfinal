<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Imagen;

class ImagesSeeder extends Seeder
{
	private $rutas = array(
		array("titulo"=> "Loro", "imagen" => "pajaro01.jpg"), 
		array("titulo"=> "Pajarraco", "imagen" =>"pajaro02.jpg"));
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	foreach ($this->rutas as $recursoNuevo) {
    		$img = new Imagen();
    		$img->titulo = $recursoNuevo["titulo"];
    		$img->imagen = $recursoNuevo["imagen"];
    		$img->save();
    	}

    	$this->command->info('Tabla de imágenes inicializada con datos');
    }
}
