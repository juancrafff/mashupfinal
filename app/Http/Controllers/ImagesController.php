<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Imagen;

class ImagesController extends Controller
{
	public function index() {
		$imagenes = Imagen::all();
		return view("images.index", compact("imagenes"));
	}

	public function show(Imagen $imagen)
	{

		$consulta = Imagen::find($imagen->id);
		return view('images.show', ["imagen" => $consulta]);
	}	

	public function create(Imagen $imagen)
	{
		return view('images.create');
	}

	public function store(Request $request)
	{
		$consulta = new Imagen();
		if ($request->hasFile("imagen")) {
			$consulta->titulo = $request->titulo;
			$consulta->imagen = $request->imagen->store('', 'imagenes');
			$consulta->save();
		}

		$tags = $this->aiConsulta($consulta, $request->confianza);
		return $tags;
		return redirect()->route("images.show", $consulta)->with("mensaje","Consulta realizada");
	}


	private function aiConsulta(Imagen $imagen, $confianza)
	{
		$file_path = 'http://localhost/mashupfinal/public/assets/imagenes/'.$imagen->imagen;
		$api_credentials = array(
			'key' => 'acc_e1bfc0dbc79ad9c',
			'secret' => '6e82c3dd3e564f8628f2d4a78b1d65ea'
		);

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, "https://api.imagga.com/v2/tags");
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
		curl_setopt($ch, CURLOPT_TIMEOUT, 60);
		curl_setopt($ch, CURLOPT_USERPWD, $api_credentials['key'].':'.$api_credentials['secret']);
		curl_setopt($ch, CURLOPT_HEADER, FALSE);
		curl_setopt($ch, CURLOPT_POST, 1);
		$fields = [
			'image' => new \CurlFile($file_path, 'image/jpeg', 'image.jpg')
		];
		curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);

		$response = curl_exec($ch);
		curl_close($ch);

		$json_response = json_decode($response);
		return var_dump($json_response);

		$tags = $json_response->result->tags;
		return $tags;
	}
}
